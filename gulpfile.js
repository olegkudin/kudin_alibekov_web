var gulp = require('gulp');
var gulpAutoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var cleanCss = require('gulp-clean-css');
var sass = require('gulp-sass');

gulp.task('html', function(){
    gulp.src('./src/*.html')
    .pipe(gulp.dest('./build/'))
    .pipe(browserSync.stream());
});

gulp.task('style', function(){
    gulp.src('./src/css/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCss())
    .pipe(gulpAutoprefixer({
        browsers: ['cover 99.5%'],
       cascade: false
           })) 
    .pipe(gulp.dest('./build/css/'))
    .pipe(browserSync.stream());       
});

gulp.task('images', function(){
    gulp.src('./src/img/**/*')
    .pipe(gulp.dest('./build/img'))
    .pipe(browserSync.stream());
});

gulp.task('script', function(){
    gulp.src('./src/JS/**/*')
    .pipe(gulp.dest('./build/JS/'))
    .pipe(browserSync.stream());
});



gulp.task('serve', ['html', 'style', 'images', 'script'], function(){
	browserSync.init({
		server: './build'
	});

    gulp.watch('./src/index.html', ['html']) 
    gulp.watch('./src/css/**/*.css', ['style'])   
    gulp.watch('./src/img/**/*', ['images'])   
    gulp.watch('./src/JS/**/*', ['script']) 
        });
              
gulp.task('default',['serve']);